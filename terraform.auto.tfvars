region = "us-east-1"

vpc_cidr = "10.0.0.0/16"

enable_dns_support = "true"

enable_dns_hostnames = "true"

enable_classiclink = "false"

enable_classiclink_dns_support = "false"

preferred_number_of_public_subnets = 2

preferred_number_of_private_subnets = 4

tags = {
  Owner-Email     = "kellyiyogun@outlook.com"
  Managed-By      = "Terraform"
  Billing-Account = "403164464781"
}

environment = "dev"

ami-web = "ami-0c56ee4f295b7621a"

ami-bastion = "ami-0e2a6973687756331"

ami-nginx = "ami-082966dfef2b9aba4"

ami-sonar = "ami-0f14f0ab24bcb475d"

keypair = "kingkellee"

# Ensure to change this to your acccount number
account_no = "403164464781"

master-username = "kingkellee"

master-password = "Password123"
